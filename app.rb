require "json"
require "openssl"
require "time"
require "uri"
require "sinatra"
require "rack/contrib"
require "http"
require "dotenv/load"

CONFIG =
  if ENV["CONFIG_JSON"] then
    config_json = ENV["CONFIG_JSON"]
    config_json = config_json[1..-1] if config_json.start_with?("'")
    config_json = config_json[0..-2] if config_json.end_with?("'")
    JSON.parse(config_json)
  else
    File.open("data/config.json", "r") do |f|
      JSON.parse(f.read)
    end
  end
ME = [
  '<a href="https://',
  URI.parse(CONFIG["actor"][0]["me"]).host,
  '/" rel="me nofollow noopener noreferrer" target="_blank">',
  "https://",
  URI.parse(CONFIG["actor"][0]["me"]).host,
  "/",
  "</a>"
].join()
private_key_pem = ENV["PRIVATE_KEY"]
private_key_pem = private_key_pem.split("\\n").join("\n")
private_key_pem = private_key_pem[1..-1] if private_key_pem.start_with?('"')
private_key_pem = private_key_pem[0..-2] if private_key_pem.end_with?('"')
PRIVATE_KEY = OpenSSL::PKey::RSA.new(private_key_pem)
public_key_pem = PRIVATE_KEY.public_key

set :environment, :production
use Rack::JSONBodyParser, verbs: ["POST"], media: ["application/activity+json"]

error Sinatra::BadRequest, 400 do
   response.status = 400
   content_type "text/html" 
   "<h1>Bad Request</h1>"
end

error Sinatra::NotFound, 404 do
   response.status = 404
   content_type "text/html" 
   "<h1>Not Found</h1>"
end

def uuidv7
  v = SecureRandom.random_bytes(16).bytes
  ts = (Time.now.to_f * 1000).to_i
  v[0] = (ts >> 40) & 0xFF
  v[1] = (ts >> 32) & 0xFF
  v[2] = (ts >> 24) & 0xFF
  v[3] = (ts >> 16) & 0xFF
  v[4] = (ts >> 8) & 0xFF
  v[5] = ts & 0xFF
  v[6] = (v[6] & 0x0F) | 0x70
  v[8] = (v[8] & 0x3F) | 0x80
  v.pack('C*').unpack1('H*')
end

def talk_script(req)
  ts = (Time.now.to_f * 1000).to_i
  return "<p>#{ts}</p>" if URI.parse(req).host == "localhost"
  [
    "<p>",
    '<a href="https://',
    URI.parse(req).host,
    '/" rel="nofollow noopener noreferrer" target="_blank">',
    URI.parse(req).host,
    "</a>",
    "</p>"
  ].join()
end

def get_activity(username, hostname, req)
  t = Time.now.utc.httpdate
  sig = PRIVATE_KEY.sign(OpenSSL::Digest::SHA256.new, [
    "(request-target): get #{URI.parse(req).path}",
    "host: #{URI.parse(req).host}",
    "date: #{t}"
  ].join("\n"))
  b64 = Base64.strict_encode64(sig)
  headers = {
    Date: t,
    Signature: [
      "keyId=\"https://#{hostname}/u/#{username}#Key\"",
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date"',
      "signature=\"#{b64}\""
    ].join(","),
    Accept: "application/activity+json",
    "Accept-Encoding": "identity",
    "Cache-Control": "no-cache",
    "User-Agent": "StrawberryFields-Sinatra/3.0.0 (+https://#{hostname}/)"
  }
  res = HTTP[headers].get(req)
  status = res.code
  puts "GET #{req} #{status}"
  JSON.parse(res)
end

def post_activity(username, hostname, req, x)
  t = Time.now.utc.httpdate
  body = x.to_json
  s256 = Digest::SHA256.base64digest(body)
  sig = PRIVATE_KEY.sign(OpenSSL::Digest::SHA256.new, [
    "(request-target): post #{URI.parse(req).path}",
    "host: #{URI.parse(req).host}",
    "date: #{t}",
    "digest: SHA-256=#{s256}"
  ].join("\n"))
  b64 = Base64.strict_encode64(sig)
  headers = {
    Date: t,
    Digest: "SHA-256=#{s256}",
    Signature: [
      "keyId=\"https://#{hostname}/u/#{username}#Key\"",
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date digest"',
      "signature=\"#{b64}\""
    ].join(","),
    Accept: "application/json",
    "Accept-Encoding": "gzip",
    "Cache-Control": "max-age=0",
    "Content-Type": "application/activity+json",
    "User-Agent": "StrawberryFields-Sinatra/3.0.0 (+https://#{hostname}/)"
  }
  puts "POST #{req} #{body}"
  HTTP[headers].post(req, body: body)
  nil
end

def accept_follow(username, hostname, x, y)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}",
    type: "Accept",
    actor: "https://#{hostname}/u/#{username}",
    object: y
  }
  post_activity username, hostname, x["inbox"], body
end

def follow(username, hostname, x)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}",
    type: "Follow",
    actor: "https://#{hostname}/u/#{username}",
    object: x["id"]
  }
  post_activity username, hostname, x["inbox"], body
end

def undo_follow(username, hostname, x)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
    type: "Undo",
    actor: "https://#{hostname}/u/#{username}",
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Follow",
      actor: "https://#{hostname}/u/#{username}",
      object: x["id"]
    }
  }
  post_activity username, hostname, x["inbox"], body
end

def like(username, hostname, x, y)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}",
    type: "Like",
    actor: "https://#{hostname}/u/#{username}",
    object: x["id"]
  }
  post_activity username, hostname, y["inbox"], body
end

def undo_like(username, hostname, x, y)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
    type: "Undo",
    actor: "https://#{hostname}/u/#{username}",
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Like",
      actor: "https://#{hostname}/u/#{username}",
      object: x["id"]
    }
  }
  post_activity username, hostname, y["inbox"], body
end

def announce(username, hostname, x, y)
  aid = uuidv7
  t = Time.now.utc.iso8601
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
    type: "Announce",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: x["id"]
  }
  post_activity username, hostname, y["inbox"], body
end

def undo_announce(username, hostname, x, y, z)
  aid = uuidv7
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
    type: "Undo",
    actor: "https://#{hostname}/u/#{username}",
    object: {
      id: "#{z}/activity",
      type: "Announce",
      actor: "https://#{hostname}/u/#{username}",
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: x["id"]
    }
  }
  post_activity username, hostname, y["inbox"], body
end

def create_note(username, hostname, x, y)
  aid = uuidv7
  t = Time.now.utc.iso8601
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
    type: "Create",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Note",
      attributedTo: "https://#{hostname}/u/#{username}",
      content: talk_script(y),
      url: "https://#{hostname}/u/#{username}/s/#{aid}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"]
    }
  }
  post_activity username, hostname, x["inbox"], body
end

def create_note_image(username, hostname, x, y, z)
  aid = uuidv7
  t = Time.now.utc.iso8601
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
    type: "Create",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Note",
      attributedTo: "https://#{hostname}/u/#{username}",
      content: talk_script("https://localhost"),
      url: "https://#{hostname}/u/#{username}/s/#{aid}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      attachment: [
        {
          type: "Image",
          mediaType: z,
          url: y
        }
      ]
    }
  }
  post_activity username, hostname, x["inbox"], body
end

def create_note_mention(username, hostname, x, y, z)
  aid = uuidv7
  t = Time.now.utc.iso8601
  at = "@#{y['preferredUsername']}@#{URI.parse(y['inbox']).host}"
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
    type: "Create",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Note",
      attributedTo: "https://#{hostname}/u/#{username}",
      inReplyTo: x["id"],
      content: talk_script(z),
      url: "https://#{hostname}/u/#{username}/s/#{aid}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      tag: [
        {
          type: "Mention",
          name: at
        }
      ]
    }
  }
  post_activity username, hostname, y["inbox"], body
end

def create_note_hashtag(username, hostname, x, y, z)
  aid = uuidv7
  t = Time.now.utc.iso8601
  body = {
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      {"Hashtag": "as:Hashtag"},
    ],
    id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
    type: "Create",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: {
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Note",
      attributedTo: "https://#{hostname}/u/#{username}",
      content: talk_script(y),
      url: "https://#{hostname}/u/#{username}/s/#{aid}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      tag: [
        {
          type: "Hashtag",
          name: "##{z}"
        }
      ]
    }
  }
  post_activity username, hostname, x["inbox"], body
end

def update_note(username, hostname, x, y)
  t = Time.now.utc.iso8601
  ts = (Time.now.to_f * 1000).to_i
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "#{y}##{ts}",
    type: "Update",
    actor: "https://#{hostname}/u/#{username}",
    published: t,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    cc: ["https://#{hostname}/u/#{username}/followers"],
    object: {
      id: y,
      type: "Note",
      attributedTo: "https://#{hostname}/u/#{username}",
      content: talk_script("https://localhost"),
      url: y,
      updated: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"]
    }
  }
  post_activity username, hostname, x["inbox"], body
end

def delete_tombstone(username, hostname, x, y)
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "#{y}#Delete",
    type: "Delete",
    actor: "https://#{hostname}/u/#{username}",
    object: {
      id: y,
      type: "Tombstone"
    }
  }
  post_activity username, hostname, x["inbox"], body
end

get "/" do
  content_type "text/plain"
  "StrawberryFields Sinatra"
end

get "/about" do
  content_type "text/plain"
  "About: Blank"
end

get "/u/:username" do |username|
  hostname = URI.parse(CONFIG["origin"]).host
  accept_header_field = request.env["HTTP_ACCEPT"]
  has_type = false
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  has_type = true if accept_header_field&.include?("application/activity+json")
  has_type = true if accept_header_field&.include?("application/ld+json")
  has_type = true if accept_header_field&.include?("application/json")
  if !has_type
    headers({
      "Cache-Control" => "public, max-age=#{CONFIG['ttl']}, must-revalidate",
      "Vary" => "Accept, Accept-Encoding"
    })
    content_type "text/plain"
    return "#{username}: #{CONFIG['actor'][0]['name']}"
  end
  body = {
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
        schema: "https://schema.org/",
        PropertyValue: "schema:PropertyValue",
        value: "schema:value",
        Key: "sec:Key"
      }
    ],
    id: "https://#{hostname}/u/#{username}",
    type: "Person",
    inbox: "https://#{hostname}/u/#{username}/inbox",
    outbox: "https://#{hostname}/u/#{username}/outbox",
    following: "https://#{hostname}/u/#{username}/following",
    followers: "https://#{hostname}/u/#{username}/followers",
    preferredUsername: username,
    name: CONFIG["actor"][0]["name"],
    summary: "<p>3.0.0</p>",
    url: "https://#{hostname}/u/#{username}",
    endpoints: {sharedInbox: "https://#{hostname}/u/#{username}/inbox"},
    attachment: [
      {
        type: "PropertyValue",
        name: "me",
        value: ME
      }
    ],
    icon: {
      type: "Image",
      mediaType: "image/png",
      url: "https://#{hostname}/public/#{username}u.png"
    },
    image: {
      type: "Image",
      mediaType: "image/png",
      url: "https://#{hostname}/public/#{username}s.png"
    },
    publicKey: {
      id: "https://#{hostname}/u/#{username}#Key",
      type: "Key",
      owner: "https://#{hostname}/u/#{username}",
      publicKeyPem: public_key_pem
    }
  }
  headers({
    "Cache-Control" => "public, max-age=#{CONFIG['ttl']}, must-revalidate",
    "Vary" => "Accept, Accept-Encoding"
  })
  content_type "application/activity+json"
  body.to_json
end

get "/u/:username/inbox" do
  405
end

post "/u/:username/inbox" do |username|
  hostname = URI.parse(CONFIG["origin"]).host
  content_type_header_field = request.env["CONTENT_TYPE"]
  has_type = false
  y = JSON.parse(request.body.read)
  t = y["type"] || ""
  aid = y["id"] || ""
  atype = y["type"] || ""
  return 400 if aid.length > 1024 or atype.length > 64
  puts "INBOX #{aid} #{atype}"
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  has_type = true if content_type_header_field&.include?("application/activity+json")
  has_type = true if content_type_header_field&.include?("application/ld+json")
  has_type = true if content_type_header_field&.include?("application/json")
  return 400 if !has_type
  return 400 if !request.env["HTTP_DIGEST"] or !request.env["HTTP_SIGNATURE"]
  return 200 if t == "Accept" or t == "Reject" or t == "Add"
  return 200 if t == "Remove" or t == "Like" or t == "Announce"
  return 200 if t == "Create" or t == "Update" or t == "Delete"
  if t == "Follow"
    return 400 if URI.parse(y["actor"] || "").scheme != "https"
    x = get_activity username, hostname, y["actor"]
    return 500 if !x
    accept_follow username, hostname, x, y
    return 200
  end
  if t == "Undo"
    z = y["object"] || {}
    t = z["type"] || ""
    return 200 if t == "Accept" or t == "Like" or t == "Announce"
    if t == "Follow"
      return 400 if URI.parse(y["actor"] || "").scheme != "https"
      x = get_activity username, hostname, y["actor"]
      return 500 if !x
      accept_follow username, hostname, x, z
      return 200
    end
  end
  500
end

post "/u/:username/outbox" do
  405
end

get "/u/:username/outbox" do |username|
  hostname = URI.parse(CONFIG["origin"]).host
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/outbox",
    type: "OrderedCollection",
    totalItems: 0
  }
  content_type "application/activity+json"
  body.to_json
end

get "/u/:username/following" do |username|
  hostname = URI.parse(CONFIG["origin"]).host
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/following",
    type: "OrderedCollection",
    totalItems: 0
  }
  content_type "application/activity+json"
  body.to_json
end

get "/u/:username/followers" do |username|
  hostname = URI.parse(CONFIG["origin"]).host
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: "https://#{hostname}/u/#{username}/followers",
    type: "OrderedCollection",
    totalItems: 0
  }
  content_type "application/activity+json"
  body.to_json
end

post "/s/:secret/u/:username" do |secret, username|
  hostname = URI.parse(CONFIG["origin"]).host
  send = JSON.parse(request.body.read)
  t = send["type"] || ""
  return 404 if username != CONFIG["actor"][0]["preferredUsername"]
  return 404 if !secret or secret == "-"
  return 404 if secret != ENV["SECRET"]
  return 400 if URI.parse(send["id"] || "").scheme != "https"
  x = get_activity username, hostname, send["id"]
  return 500 if !x
  aid = x["id"] || ""
  atype = x["type"] || ""
  return 400 if aid.length > 1024 or atype.length > 64
  if t == "follow"
    follow username, hostname, x
    return 200
  end
  if t == "undo_follow"
    undo_follow username, hostname, x
    return 200
  end
  if t == "like"
    return 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
    y = get_activity username, hostname, x["attributedTo"]
    return 500 if !y
    like username, hostname, x, y
    return 200
  end
  if t == "undo_like"
    return 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
    y = get_activity username, hostname, x["attributedTo"]
    return 500 if !y
    undo_like username, hostname, x, y
    return 200
  end
  if t == "announce"
    return 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
    y = get_activity username, hostname, x["attributedTo"]
    return 500 if !y
    announce username, hostname, x, y
    return 200
  end
  if t == "undo_announce"
    return 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
    y = get_activity username, hostname, x["attributedTo"]
    return 500 if !y
    if send["url"] && !send["url"].empty?
      z = send["url"]
    else
      z = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
    end
    return 400 if URI.parse(z).scheme != "https"
    undo_announce username, hostname, x, y, z
    return 200
  end
  if t == "create_note"
    if send["url"] && !send["url"].empty?
      y = send["url"]
    else
      y = "https://localhost"
    end
    return 400 if URI.parse(y).scheme != "https"
    create_note username, hostname, x, y
    return 200
  end
  if t == "create_note_image"
    if send["url"] && !send["url"].empty?
      y = send["url"]
    else
      y = "https://#{hostname}/public/logo.png"
    end
    return 400 if URI.parse(y).scheme != "https" or URI.parse(y).host != hostname
    z =
      if y.end_with?(".jpg") or y.end_with?(".jpeg")
        "image/jpeg"
      elsif y.end_with?(".svg")
        "image/svg+xml"
      elsif y.end_with?(".gif")
        "image/gif"
      elsif y.end_with?(".webp")
        "image/webp"
      elsif y.end_with?(".avif")
        "image/avif"
      else
        "image/png"
      end
    create_note_image username, hostname, x, y, z
    return 200
  end
  if t == "create_note_mention"
    return 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
    y = get_activity username, hostname, x["attributedTo"]
    return 500 if !y
    if send["url"] && !send["url"].empty?
      z = send["url"]
    else
      z = "https://localhost"
    end
    return 400 if URI.parse(z).scheme != "https"
    create_note_mention username, hostname, x, y, z
    return 200
  end
  if t == "create_note_hashtag"
    if send["url"] && !send["url"].empty?
      y = send["url"]
    else
      y = "https://localhost"
    end
    return 400 if URI.parse(y).scheme != "https"
    if send["tag"] && !send["tag"].empty?
      z = send["tag"]
    else
      z = "Hashtag"
    end
    create_note_hashtag username, hostname, x, y, z
    return 200
  end
  if t == "update_note"
    if send["url"] && !send["url"].empty?
      y = send["url"]
    else
      y = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
    end
    return 400 if URI.parse(y).scheme != "https"
    update_note username, hostname, x, y
    return 200
  end
  if t == "delete_tombstone"
    if send["url"] && !send["url"].empty?
      y = send["url"]
    else
      y = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
    end
    return 400 if URI.parse(y).scheme != "https"
    delete_tombstone username, hostname, x, y
    return 200
  end
  puts "TYPE #{aid} #{atype}"
  200
end

get "/.well-known/nodeinfo" do
  hostname = URI.parse(CONFIG["origin"]).host
  body = {
    links: [
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.0",
        href: "https://#{hostname}/nodeinfo/2.0.json"
      },
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.1",
        href: "https://#{hostname}/nodeinfo/2.1.json"
      }
    ]
  }
  headers({
    "Cache-Control" => "public, max-age=#{CONFIG['ttl']}, must-revalidate",
    "Vary" => "Accept, Accept-Encoding"
  })
  body.to_json
end

get "/.well-known/webfinger" do
  username = CONFIG["actor"][0]["preferredUsername"]
  hostname = URI.parse(CONFIG["origin"]).host
  p443 = "https://#{hostname}:443/"
  resource = params[:resource]
  has_resource = false
  resource = "https://#{hostname}/#{resource[p443.length..-1]}" if resource.start_with?(p443)
  has_resource = true if resource == "acct:#{username}@#{hostname}"
  has_resource = true if resource == "mailto:#{username}@#{hostname}"
  has_resource = true if resource == "https://#{hostname}/@#{username}"
  has_resource = true if resource == "https://#{hostname}/u/#{username}"
  has_resource = true if resource == "https://#{hostname}/user/#{username}"
  has_resource = true if resource == "https://#{hostname}/users/#{username}"
  return 404 if !has_resource
  body = {
    subject: "acct:#{username}@#{hostname}",
    aliases: [
      "mailto:#{username}@#{hostname}",
      "https://#{hostname}/@#{username}",
      "https://#{hostname}/u/#{username}",
      "https://#{hostname}/user/#{username}",
      "https://#{hostname}/users/#{username}"
    ],
    links: [
      {
        rel: "self",
        type: "application/activity+json",
        href: "https://#{hostname}/u/#{username}"
      },
      {
        rel: "http://webfinger.net/rel/avatar",
        type: "image/png",
        href: "https://#{hostname}/public/#{username}u.png"
      },
      {
        rel: "http://webfinger.net/rel/profile-page",
        type: "text/plain",
        href: "https://#{hostname}/u/#{username}"
      }
    ]
  }
  headers({
    "Cache-Control" => "public, max-age=#{CONFIG['ttl']}, must-revalidate",
    "Vary" => "Accept, Accept-Encoding"
  })
  content_type "application/jrd+json"
  body.to_json
end

get ["/@", "/u", "/user", "/users"] do
  redirect "/"
end

get ["/users/:username", "/user/:username", "/@:username"] do |username|
  redirect "/u/#{username}"
end
