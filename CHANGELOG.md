# StrawberryFields Sinatra

- [3.0.0]
- [2.9.0] - 2024-08-10 - 7 files changed, 149 insertions(+), 93 deletions(-)
- [2.8.0] - 2024-03-10 - 2 files changed, 89 insertions(+), 25 deletions(-)
- [2.7.0] - 2024-02-04 - 5 files changed, 67 insertions(+), 35 deletions(-)
- [2.6.0] - 2023-11-11 - 5 files changed, 30 insertions(+), 20 deletions(-)
- [2.5.0] - 2023-11-05 - 12 files changed, 210 insertions(+), 126 deletions(-)
- [2.4.0] - 2023-04-16 - 7 files changed, 100 insertions(+), 57 deletions(-)
- [2.3.0] - 2022-11-27 - 9 files changed, 98 insertions(+), 12 deletions(-)
- [2.2.0] - 2022-11-20 - 6 files changed, 10 insertions(+), 8 deletions(-)
- [2.1.0] - 2022-06-26 - 4 files changed, 13 insertions(+), 23 deletions(-)
- [2.0.0] - 2022-03-13 - 2 files changed, 216 insertions(+), 17 deletions(-)
- [1.3.0] - 2021-10-11 - 2 files changed, 46 insertions(+), 3 deletions(-)
- [1.2.0] - 2021-10-01 - 3 files changed, 9 insertions(+), 2 deletions(-)
- [1.1.0] - 2021-09-25 - 5 files changed, 38 insertions(+), 32 deletions(-)
- 1.0.0 - 2021-04-29

[3.0.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/62c1e85f...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/fae13eab...62c1e85f
[2.8.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/38820506...fae13eab
[2.7.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/a2f2b323...38820506
[2.6.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/acbece62...a2f2b323
[2.5.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/b75c6b2c...acbece62
[2.4.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/3ae5f063...b75c6b2c
[2.3.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/67ad1982...3ae5f063
[2.2.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/ebd2d34f...67ad1982
[2.1.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/db197de2...ebd2d34f
[2.0.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/a4d2e249...db197de2
[1.3.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/315b98c4...a4d2e249
[1.2.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/3a22d163...315b98c4
[1.1.0]: https://gitlab.com/acefed/strawberryfields-sinatra/-/compare/8c7ac0ed...3a22d163
