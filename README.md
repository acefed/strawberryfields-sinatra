# StrawberryFields Sinatra

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/acefed/strawberryfields-sinatra)

[https://acefed.gitlab.io/strawberryfields](https://acefed.gitlab.io/strawberryfields)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/acefed/strawberryfields-sinatra/-/raw/master/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
StrawberryFields Sinatra
```

[https://acefed.gitlab.io/strawberryfields/strawberryfields-sinatra.spdx](https://acefed.gitlab.io/strawberryfields/strawberryfields-sinatra.spdx)  
[https://acefed.gitlab.io/strawberryfields/strawberryfields-sinatra.json](https://acefed.gitlab.io/strawberryfields/strawberryfields-sinatra.json)

SPDX-License-Identifier: MIT
